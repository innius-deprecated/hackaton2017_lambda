'use strict';

const simpleParser = require('mailparser').simpleParser;
const https = require('https');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const sql = require('./sqldatabase');


/**
 * Pass the data to send as `event.data`, and the request options as
 * `event.options`. For more information see the HTTPS module documentation
 * at https://nodejs.org/api/https.html.
 *
 * Will succeed with the response body.
 */

exports.handler = (event, context, callback) => {
    console.log("the body", JSON.stringify(event));
    
    const bucket = event.Records[0].s3.bucket;
    const object = event.Records[0].s3.object;
    s3.getObject({Bucket : bucket.name, Key: object.key}, (err, source) => {
        if (err) return callback(err);
        
        simpleParser(source.Body, (err, mail) => {
            if (err) return callback(err);
            processMessage(mail, callback);
        });
    });
}

function processMessage(mail, callback) {
    console.log(JSON.stringify(mail));    
    let mail2 = {};
    mail2.From = mail.from.value[0];
    mail2.To = mail.to.value[0];
    mail2.DateTime = mail.date;
    mail2.Subject = mail.subject;
    mail2.Customer = mail.to.text.split("@")[1];
    mail2.Contact = mail.to.text[0].split("@")[0];
    if (mail2.Customer == mail.from.text.split("@")[1]) {
        mail2.IsInternal = true;
    } else {
        mail2.IsInternal = false;
    }
        
    let options = {
        hostname : "westus.api.cognitive.microsoft.com",
        path : "/text/analytics/v2.0/sentiment",
        method: "POST",
        headers : {
            "Ocp-Apim-Subscription-Key" : process.env.subscription_key,
            "Content-Type" : "application/json", 
            "Accept" : "application/json"
        }
    };

    const language = "en"

    let documents = [
        {
            language: language,
            id: "body",
            text: mail.text
        },
        {
            language: language,
            id: "subject",
            text: mail.subject
        }
    ]

    console.log("Calling Azure Cognitive Services")
    const req = https.request(options, (res) => {
        let body = '';

        console.log('Status:', res.statusCode);
        console.log('Headers:', JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', (chunk) => body += chunk);
        res.on('end', () => {
            let respbody = {};
            console.log('Successfully processed HTTPS response');
            if (res.headers['content-type'].startsWith('application/json')) {
                respbody = JSON.parse(body);
            }
            mail2.Sentiment = respbody.documents[0].score;
            sql.insertMail(mail2);
            callback(null, respbody);
        });
    });

    req.on('error', callback);
    req.write(JSON.stringify({ documents: documents }));
    req.end();
};

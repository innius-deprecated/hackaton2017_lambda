var grunt = require('grunt');

module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-aws-lambda');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.loadNpmTasks('grunt-env');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
				files: ['Gruntfile.js', 'src/**/*.js', '**/*.js', '*.js'],
				options: {
						globals: {
								jQuery: true
								
						},
						esnext: true
				}
		},
		env: {
            test: {
                AWS_LAMBDA_FUNCTION_NAME: 'chair-lambda-KinesisToRedisLambda-ckmir8eaee',
				TESTING: true,
            }
        },
		mochaTest: {
			test: {
					options: {
							reporter: 'spec',
							quiet: false, // Optionally suppress output to standard out (defaults to false)
							clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
					},
					src: ['test/**/*.js']
			}
		},
		lambda_invoke: {
			default : {
			}
		},
		lambda_package: {
			default:{
				options: {
					include_time : false
				}
			}
		},
		lambda_deploy: {
			default: {
				options: {
					// Task-specific options go here.
					region: 'us-east-1'
				},
				//arn: 'arn:aws:lambda:us-east-1:831844703282:function:xenon-analytics-service-EventServiceLambda-1FKG0UJ4EKM5K'
				arn: ''
			}
		}
	});

	grunt.registerTask('default', ['build']);
	grunt.registerTask('test', ['env:test', 'mochaTest']);
	grunt.registerTask('build', ['test', 'lambda_package']);
	grunt.registerTask('deploy', ['build', 'lambda_deploy']);
};

var Connection = require('tedious').Connection;
var Request = require('tedious').Request;


// Create connection to database
var config = {
  userName: process.env.username, // update me
  password: process.env.password, // update me
  server: process.env.server, // update me
  options: {
      encrypt: true,
      database: 'hacketon' //update me
  }
}
var connection = new Connection(config);

// Attempt to connect and execute queries if connection goes through
connection.on('connect', function(err) {
    if (err) {
        console.log(err)
    }
});

// [‎4/‎7/‎2017 5:46 PM] Henry van Butselaar (TI): 
// /****** Script for SelectTopNRows command from SSMS  ******/
// SELECT TOP (1000) 
//        [DateTime]
//       ,[Message]
//       ,[Subject]
//       ,[Customer]
//       ,[Contact]
//       ,[Sentiment]
//       ,[IsInternal]
//       ,[From]
//       ,[To]
//   FROM [dbo].[TIEmailAnalysis] 


exports.insertMail = (mail) => {
    console.log("Inserting mail");
    request = new Request(
        "INSERT INTO TIEmailAnalysis (DateTime, Message, Subject, Customer, Contact, Sentiment, IsInternal, From, To) VALUES (" + mail.DateTime + "," + mail.Message + "," + mail.Subject + "," + mail.Customer + "," + mail.Contact + "," + mail.Sentiment + "," + mail.IsInternal + "," + mail.From + "," + mail.To + ")",
        function(err, rowCount, rows) {
            console.log(rowCount + ' row(s) inserted');
        }
    );
    connection.execSql(request);
}